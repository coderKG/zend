-- Table: public.book

-- DROP TABLE public.book;

CREATE TABLE public.book
(
    id bigint NOT NULL DEFAULT nextval('book_id_seq'::regclass),
    name character varying(255) COLLATE pg_catalog."default",
    quantity integer NOT NULL DEFAULT 1,
    in_rent integer NOT NULL DEFAULT 0,
    CONSTRAINT book_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.book
    OWNER to postgres;
    
    -- Table: public."user"

-- DROP TABLE public."user";

CREATE TABLE public."user"
(
    id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    name character varying COLLATE pg_catalog."default",
    qt_books integer NOT NULL DEFAULT 0,
    CONSTRAINT user_pk PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public."user"
    OWNER to postgres;
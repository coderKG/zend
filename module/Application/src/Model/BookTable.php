<?php

namespace Application\Model;

use Zend\Db\TableGateway\TableGatewayInterface;

class BookTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        return $this->tableGateway->select();
    }

    public function select($where)
    {
        return $this->tableGateway->select($where);
    }

    /**
     * @param $id
     * @return Book
     */
    public function getBook($id)
    {
        $id = (int)$id;
        $rowset = $this->tableGateway->select(['id' => $id]);
        $row = $rowset->current();
        if (!$row) {
            throw new \Zend\Db\Exception\RuntimeException(sprintf(
                'Could not find book with identifier %d',
                $id
            ));
        }

        return $row;
    }

    public function saveBook(Book $book)
    {
        $data = [
            'name' => $book->name,
            'quantity' => $book->getQuantity(),
            'in_rent' => (int)$book->getRentQuantity()
        ];

        $id = (int)$book->id;

        if ($id === 0) {
            $this->tableGateway->insert($data);
            return;
        }

        try {
            $this->getBook($id);
        } catch (\Zend\Db\Exception\RuntimeException $e) {
            throw new \Zend\Db\Exception\RuntimeException(sprintf(
                'Cannot update album with identifier %d; does not exist',
                $id
            ));
        }

        $this->tableGateway->update($data, ['id' => $id]);
    }

    public function deleteBook($id)
    {
        $this->tableGateway->delete(['id' => (int)$id]);
    }
}
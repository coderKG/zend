<?php

namespace Application\Form;

use Zend\Form\Element\Select;
use Zend\Form\Form;

class UserBookForm extends Form
{
    public function __construct()
    {
        // We will ignore the name provided to the constructor
        parent::__construct('UserBook');

        $this->setAttribute('method', 'POST');

        $this->add([
            'name' => 'redirect',
            'type' => 'hidden',
        ]);

        $select = new Select();
        $select->setLabel('Пользователь')
            ->setName('user')
            ->setAttribute('required', true)
            ->setEmptyOption('Выберите пользователя');

        $this->add($select);

        $select = new Select();
        $select->setLabel('Выдать книгу')
            ->setName('book')
            ->setAttribute('required', true)
            ->setEmptyOption('Выберите книгу')
            ->setAttribute('class', 'userBook');

        $this->add($select);

        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Выдать',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}
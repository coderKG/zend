<?php

namespace Application\Form;

use Zend\Form\Form;

class BookForm extends Form
{
    public function __construct($name = null)
    {
        // We will ignore the name provided to the constructor
        parent::__construct('book');

        $this->setAttribute('method', 'POST');

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
        $this->add([
            'name' => 'name',
            'type' => 'text',
            'options' => [
                'label' => 'Имя',
            ],
        ]);
        $this->add([
            'name' => 'quantity',
            'type' => 'number',
            'options' => [
                'label' => 'Количество шт',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Добавить',
                'id'    => 'submitbutton',
            ],
        ]);
    }
}
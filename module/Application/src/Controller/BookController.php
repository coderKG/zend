<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Form\BookForm;
use Application\Form\UserBookForm;
use Application\Model\Book;
use Application\Model\BookTable;
use Application\Model\UserTable;
use Zend\Db\Exception\RuntimeException;
use Zend\Db\ResultSet\ResultSet;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Zend\View\Model\ViewModel;

class BookController extends AbstractActionController
{
    private $table;
    private $userTable;
    private $messenger;

    public function __construct(BookTable $table, UserTable $userTable)
    {
        $this->table = $table;
        $this->userTable = $userTable;
        $this->messenger = new FlashMessenger();
    }

    public function indexAction()
    {
        return (new ViewModel([
            'books' => $this->table->fetchAll()
        ]));
    }

    public function addAction()
    {
        $form = new BookForm();

        $request = $this->getRequest();

        if (! $request->isPost()) {

            return [
                'form' => $form
            ];
        }

        $book = new Book();
        $form->setInputFilter($book->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return ['form' => $form];
        }

        $book->exchangeArray($form->getData());
        $this->table->saveBook($book);
        $this->messenger->addSuccessMessage('Книга добавлена');
        return $this->redirect()->toRoute('book');
    }

    public function editAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('book', ['action' => 'add']);
        }

        try {
            $book = $this->table->getBook($id);
        } catch (\Exception $e) {
            return $this->redirect()->toRoute('album', ['action' => 'index']);
        }

        $form = new BookForm();
        $form->bind($book);
        $form->get('submit')->setAttribute('value', 'Сохранить');

        $request = $this->getRequest();
        $viewData = ['id' => $id, 'action' => 'edit', 'form' => $form, 'template' => 'add'];

        if (! $request->isPost()) {
            return (new ViewModel($viewData))->setTemplate('application/book/add');
        }

        $form->setInputFilter($book->getInputFilter());
        $form->setData($request->getPost());

        if (! $form->isValid()) {
            return $viewData;
        }

        $this->table->saveBook($book);
        $this->messenger->addSuccessMessage('Данные книги обновлены!');

        // Redirect to album list
        return $this->redirect()->toRoute('book', ['action' => 'index']);
    }

    public function deleteAction()
    {
        $id = (int) $this->params()->fromRoute('id', 0);

        if (0 === $id) {
            return $this->redirect()->toRoute('book', ['action' => 'add']);
        }

        try {
            $book = $this->table->getBook($id);
            $this->table->deleteBook($id);
            $this->messenger->addSuccessMessage('Книга успешно удалена');
        } catch (\Zend\Db\Exception\RuntimeException $e) {
            $this->messenger->addErrorMessage($e->getMessage());
        } catch (\Exception $ex) {
            $this->messenger->addErrorMessage('Ошибка при удалении');
        }

        // Redirect to album list
        return $this->redirect()->toRoute('book', ['action' => 'index']);
    }

    public function rentAction()
    {
        $form = new UserBookForm();

        $submitButton = $form->get('submit');
        $bookField = $form->get('book');

        $books = $this->table->select(['quantity > 0']);
        if ($books->count() == 0) {
            $bookField->setEmptyOption('Нету свободных книг')
                ->setAttribute('disabled', 'true');
            $submitButton->setAttribute('disabled', 'true');
        } else {
            $options = [];
            foreach ($books as $book) {
                $options[$book->id] = $book->name . "(x{$book->getQuantity()})";
            }
            $bookField->setValueOptions($options);
        }

        $userField = $form->get('user');

        $users = $this->userTable->fetchAll();
        if ($users->count() == 0) {
            $userField->setEmptyOption('-- пусто --')
                ->setAttribute('disabled', 'true');
            $submitButton->setAttribute('disabled', 'true');
        } else {
            $options = [];
            foreach ($users as $user) {
                $options[$user->id] = $user->name;
            }
            $userField->setValueOptions($options);
        }

        $request = $this->getRequest();
        if (! $request->isPost()) {
            $bookId = (int)$this->params()->fromRoute('id');
            $userId = (int)$this->params()->fromRoute('user-id');
            if ($bookId) {
                $bookField->setValue($bookId);
                $form->get('redirect')->setValue('book');
            }
            if ($userId) {
                $userField->setValue($userId);
                $form->get('redirect')->setValue('user');
            }
            return ['form' => $form];
        }


        try {
            $bookId = (int)$this->params()->fromPost('book');
            $userId = (int)$this->params()->fromPost('user');

            $book = $this->table->getBook($bookId);
            $user = $this->userTable->getUser($userId);

            $book->giveToRent();
            $this->table->saveBook($book);

            $user->addBook();
            $this->userTable->saveUser($user);

            $this->messenger->addSuccessMessage('Книга успешно выдана');
        } catch (RuntimeException $ex) {
            //book or user not found
            $this->messenger->addErrorMessage($ex->getMessage());
        }

        $redirect = $this->params()->fromPost('redirect');
        if (in_array($redirect, ['book', 'user'])) {
            return $this->redirect()->toRoute($redirect);
        } else {
            return $this->redirect()->toRoute('book', ['action' => 'rent']);
        }
    }
}
